#include <Servo.h>

// ---------PinOut---------//

int BtnA = 6;
int JoyAx = A1; 
int JoyAy = A0; 

int BtnB = 5;
int JoyBx = A2; 
int JoyBy = A3; 

int ServoCurrent = A4;

int HomePin = 3;
int StartPin = 2;
int SpeedButton = 4;

// ---------BtnJoyValues---------//

boolean HomePinVal = LOW;
boolean StartPinVal = LOW;
boolean SpeedBtn = HIGH;

boolean BtnAVal = LOW;
int JoyAxVal = 0; 
int JoyAyVal = 0; 

boolean BtnBVal = LOW;
int JoyBxVal = 0; 
int JoyByVal = 0; 

int JoyDead = 7; 

boolean JoyAxActiv = false;
boolean JoyAyActiv = false;
boolean JoyBxActiv = false;
boolean JoyByActiv = false;
boolean BtnA_Activ = false;
boolean BtnB_Activ = false;


// ---------ServoSettings---------//

//table limiter for protect table
int tableLimiter = 1880;

int servo0Pos = 90;                   //NESAHAT !!!
int servo1Pos = 180;                   //NESAHAT !!! 
int servo2Pos = 20;                   //NESAHAT !!!
int servo3Pos = 80;                   //NESAHAT !!!
int servo4Pos = 100;                   //NESAHAT !!! 
int servo5Pos = 0;                   //NESAHAT !!!  

int servo0DefPos = 90; 
int servo1DefPos = 180; 
int servo2DefPos = 20; 
int servo3DefPos = 80;
int servo4DefPos = 100; 
int servo5DefPos = 0;  
/*
unsigned long Axis0Limiter = 30;     //limit ve "°" levo/pravo
unsigned long Axis1Limiter = 60;
unsigned long Axis2Limiter = 60;
unsigned long Axis3Limiter = 50;
unsigned long Axis4Limiter = 0;
*/
unsigned long Axis0Limiter = 30;     //limit ve "°" levo/pravo
unsigned long Axis1Limiter = 50;
unsigned long Axis2Limiter = 25;
unsigned long Axis3Limiter = 40;
unsigned long Axis4Limiter = 0;

Servo servo0;                        //NESAHAT !!!
Servo servo1;                        //NESAHAT !!!
Servo servo2;                        //NESAHAT !!!
Servo servo3;                        //NESAHAT !!!
Servo servo4;                        //NESAHAT !!!
Servo servo5;                        //NESAHAT !!!


//----------CatchSettings----------------//
int IsDef = 45;
int IsAct = 0;

int CatchStrength = 20;
//----------SomeUsefulShit--------------//
boolean catchDetect = false;
boolean catchDone = false;
int SmoothIsAct = 0;
int SmoothLevel = 4;

int SmoothCount = 0;
// --------- MillisBtnDebouncer---------//

int buttonState;             
int lastButtonState = HIGH;   
unsigned long lastDebounceTime = 0; 
unsigned long debounceDelay = 100;           //NESAHAT !!!

unsigned long previousMillis = 0;    
const long interval = 10;


// --------- MoveDelay---------//

unsigned long Axis0Per = 0;
unsigned long Axis1Per = 0;
unsigned long Axis2Per = 0;
unsigned long Axis3Per = 0;
unsigned long Axis4Per = 0;


unsigned long Axis0Cur = 0;                   //NESAHAT !!!
unsigned long Axis1Cur = 0;
unsigned long Axis2Cur = 0;
unsigned long Axis3Cur = 0;
unsigned long Axis4Cur = 0;

unsigned long Axis0Interval = 0;
unsigned long Axis1Interval = 0;
unsigned long Axis2Interval = 0;
unsigned long Axis3Interval = 0;
unsigned long Axis4Interval = 0;

// --------- MoveSpeed---------//

int MaxSpeed = 35;     //Lower => higher speed .... delay mezi kroky os
int MinSpeed = 45;

int ButtonSpeed = 35 ; //When speedButton pressed

    
void setup() {

  servo0.attach(7);
  servo1.attach(8);
  servo2.attach(9);
  servo3.attach(10);
  servo4.attach(11);
  servo5.attach(12);
  
  servo0.write(servo0Pos);
  servo1.write(servo1Pos);
  servo2.write(servo2Pos);
  servo3.write(servo3Pos);
  servo4.write(servo4Pos);
  servo5.write(servo5Pos);
  
  pinMode(BtnA, INPUT);
  pinMode(BtnB, INPUT);
  pinMode(HomePin, INPUT);
  pinMode(StartPin, INPUT);
  pinMode(SpeedButton, INPUT);

  digitalWrite(BtnA, HIGH);
  digitalWrite(BtnB, HIGH);
  digitalWrite(HomePin, HIGH);
  digitalWrite(StartPin, HIGH);
  digitalWrite(SpeedButton, HIGH);

  Serial.begin(250000);


}

void loop() {
  joyread();
  btnread();
 if(StartPinVal == LOW){
  
  joystate(JoyDead);
  getServoCurrent();
  movement(); 
  //Serial.println(JoyAxVal);
 }
 //Serial.println(catchDone);
 if(HomePinVal == LOW)calStart();



}


void calStart(){
  
  servo0.write(servo0DefPos);
  servo1.write(servo1DefPos);
  servo2.write(servo2DefPos);
  servo3.write(servo3DefPos);
  servo4.write(servo4DefPos);
  servo5.write(servo5DefPos);

  servo0Pos = servo0DefPos;
  servo1Pos = servo1DefPos;
  servo2Pos = servo2DefPos;
  servo3Pos = servo3DefPos;
  servo4Pos = servo4DefPos;
  servo5Pos = servo5DefPos;
  };


void calibrate(){
  int CalDone = 0;
      while(CalDone < 7){ 
        if(servo0Pos < servo0DefPos){
            servo0Pos++;
            servo0.write(servo0Pos);
          }
        if(servo0Pos > servo0DefPos){
            servo0Pos--;
            servo0.write(servo0Pos);
          }
        if(servo0Pos == servo0DefPos)CalDone++;
    
      //------------------------------------------
        if(servo1Pos < servo1DefPos){
            servo1Pos++;
            servo1.write(servo1Pos);
          }
        if(servo1Pos > servo1DefPos){
            servo1Pos--;
            servo1.write(servo1Pos);
        }
        if(servo1Pos == servo1DefPos)CalDone++;
      //----------------------------------------
        if(servo2Pos < servo2DefPos){
            servo2Pos++;
            servo2.write(servo2Pos);
          }
        if(servo2Pos > servo2DefPos){
            servo2Pos--;
            servo2.write(servo2Pos);
          }
          if(servo2Pos == servo2DefPos)CalDone++;
       //------------------------------------------
        if(servo3Pos < servo3DefPos){
            servo3Pos++;
            servo3.write(servo3Pos);
          }
        if(servo3Pos > servo3DefPos){
            servo3Pos--;
            servo3.write(servo3Pos);     
        }
        if(servo3Pos == servo3DefPos)CalDone++;
      //------------------------------------------
      if(servo4Pos < servo4DefPos){
            servo4Pos++;
            servo4.write(servo4Pos);
          }
        if(servo4Pos > servo4DefPos){
            servo4Pos--;
            servo4.write(servo4Pos);     
        }
        if(servo4Pos == servo4DefPos)CalDone++;
      //------------------------------------------
      if(servo5Pos < servo5DefPos){
            servo5Pos++;
            servo5.write(servo5Pos);
          }
        if(servo5Pos > servo5DefPos){
            servo5Pos--;
            servo5.write(servo5Pos);
        }
        if(servo5Pos == servo5DefPos)CalDone++;
     delay(15); // Das ist mein first "delay" here...
     }     
  }
  
void btnread(){

  HomePinVal = digitalRead(HomePin);
  StartPinVal = digitalRead(StartPin);

  SpeedBtn = digitalRead(SpeedButton);
  
 if(catchDone == true){
  BtnAVal = digitalRead(BtnA);
  
 }
  BtnBVal = digitalRead(BtnB);


  if (BtnAVal != lastButtonState) {
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {

    if (BtnAVal != buttonState) {
      buttonState = BtnAVal;

      if (buttonState == HIGH) {
          BtnA_Activ = !BtnA_Activ;
          Serial.println("catchActivated!!!");
      }
    }
  }
  lastButtonState = BtnAVal;
  
  }

void joyread(){

  JoyAxVal = map(analogRead(JoyAx), 0, 1023, -100,100 );
  //Serial.println(JoyAxVal);
  JoyAyVal = map(analogRead(JoyAy), 0, 1023, -100,100 )*-1;
  //Serial.println(JoyAyVal);
  JoyBxVal = map(analogRead(JoyBx), 0, 1023, -100,100 );
  //Serial.println(JoyBxVal);
  JoyByVal = map(analogRead(JoyBy), 0, 1023, -100,100 );
  //Serial.println(JoyByVal);
  //Serial.println("----------------");
 
  }


  void servoPrint(){
    
 Serial.println("-------");
 Serial.print(" servo1: ");
 Serial.print(servo1Pos);
 Serial.print(" servo2: ");
 Serial.print(servo2Pos);
 Serial.print(" servo3: ");
 Serial.println(servo3Pos);
 Serial.println("-------");
 Serial.println("soucet: ");
 Serial.println((180-servo1Pos)*10+(180-servo2Pos)*6+(servo3Pos)*3);
    
    }

void joystate(int deadPos){
    

    
    if(abs(JoyAxVal)>=deadPos){
    JoyAxActiv = true;
    servoPrint();

   }
   else{
    JoyAxActiv = false;
    }
    
    if(abs(JoyAyVal)>=deadPos){
    JoyAyActiv = true;
    servoPrint();
 
    }
    else{
    JoyAyActiv = false;
      }

    if(abs(JoyBxVal)>=deadPos){
    JoyBxActiv = true;
  servoPrint();
    }
    else{
    JoyBxActiv = false;
      } 
    
    if(abs(JoyByVal)>=deadPos){
    JoyByActiv = true;
  servoPrint();

    }
    else{
    JoyByActiv = false;
      }

     if(BtnBVal==HIGH){
    BtnB_Activ = true;
    }
    else{
    BtnB_Activ = false;
      }

  }

void getServoCurrent(){
  IsAct = analogRead(ServoCurrent);
  }

void movement(){
      
    if(JoyAxActiv == true){
      if(SpeedBtn == LOW)Axis0Interval = ButtonSpeed;
         else Axis0Interval = map(abs(JoyAxVal), 0, 100, MinSpeed, MaxSpeed);
       //Axis0Interval = map(abs(JoyAxVal), 0, 100, MinSpeed, MaxSpeed);
        if(JoyAxVal > 0 && servo0Pos <= (180 - Axis0Limiter)){
        
      
        
            Axis0Cur = millis();

                 if (Axis0Cur - Axis0Per >= Axis0Interval) {
    
                  servo0Pos++;
                  servo0.write(servo0Pos);
                  Axis0Per = Axis0Cur;
                  
                } 
            
    }
        if(JoyAxVal < 0 && servo0Pos >= (0 + Axis0Limiter)){
          
        
                 Axis0Cur = millis();

                  if (Axis0Cur - Axis0Per >= Axis0Interval) {
                   servo0Pos--;
                   servo0.write(servo0Pos);
                   Axis0Per = Axis0Cur;
                  
                } 
          
    }
  }
      if(JoyAyActiv == true){
        if(SpeedBtn == LOW)Axis1Interval = ButtonSpeed;      
          else Axis1Interval = map(abs(JoyAyVal), 0, 100, MinSpeed, MaxSpeed);
       //Axis1Interval = map(abs(JoyAyVal), 0, 100, MinSpeed, MaxSpeed);
        if(JoyAyVal > 0 && servo1Pos <= (180 - Axis1Limiter) ){
         
        
                 Axis1Cur = millis();

                  if (Axis1Cur - Axis1Per >= Axis1Interval) {
                    servo1Pos++;
                    servo1.write(servo1Pos);
                    Axis1Per = Axis1Cur;
                  
                } 
          }
        if(JoyAyVal < 0 && servo1Pos >= (0 + Axis1Limiter)&& ((180-servo1Pos)*10+(180-servo2Pos)*6+(servo3Pos)*3)<=tableLimiter){
          
        
                 Axis1Cur = millis();

                  if (Axis1Cur - Axis1Per >= Axis1Interval) {
                      servo1Pos--;
                      servo1.write(servo1Pos);
                      Axis1Per = Axis1Cur;
                                      
                } 
          }
    }


    
    if(JoyBxActiv == true){
      if(SpeedBtn == LOW)Axis2Interval = ButtonSpeed;
        else Axis2Interval = map(abs(JoyBxVal), 0, 100, MinSpeed, MaxSpeed);
       //Axis2Interval = map(abs(JoyBxVal), 0, 100, MinSpeed, MaxSpeed);
        if(JoyBxVal > 0 && servo2Pos <= (180 - Axis2Limiter) ){
          
           Axis2Cur = millis();

                  if (Axis2Cur - Axis2Per >= Axis2Interval) {
                    servo2Pos++;
                    servo2.write(servo2Pos);
                    Axis2Per = Axis2Cur;                        
                } 
          }
        if(JoyBxVal < 0 && servo2Pos >= (0 + Axis2Limiter) && ((180-servo1Pos)*10+(180-servo2Pos)*6+(servo3Pos)*3)<=tableLimiter){
                Axis2Cur = millis();

                  if (Axis2Cur - Axis2Per >= Axis2Interval) {
                    servo2Pos--;
                    servo2.write(servo2Pos);
                    Axis2Per = Axis2Cur;                        
                } 
          }
    }
      if(JoyByActiv == true && BtnBVal == HIGH ){
        if(SpeedBtn == LOW)Axis3Interval = ButtonSpeed;
        else Axis3Interval = map(abs(JoyByVal), 0, 100, MinSpeed, MaxSpeed);
          //Axis3Interval = map(abs(JoyByVal), 0, 100, MinSpeed, MaxSpeed);
        if(JoyByVal > 0 && servo3Pos <= (180 - Axis3Limiter) && ((180-servo1Pos)*10+(180-servo2Pos)*6+(servo3Pos)*3)<=tableLimiter){
            Axis3Cur = millis();

                  if (Axis3Cur - Axis3Per >= Axis3Interval) {
                    servo3Pos++;
                    servo3.write(servo3Pos);
                    Axis3Per = Axis3Cur;                        
                } 
          }
        if(JoyByVal < 0 && servo3Pos >= (0 + Axis3Limiter) ){
             Axis3Cur = millis();

                  if (Axis3Cur - Axis3Per >= Axis3Interval) {
                    servo3Pos--;
                    servo3.write(servo3Pos);
                    Axis3Per = Axis3Cur;                        
                } 
          }
    }
      if(JoyByActiv == true && BtnBVal == LOW){
        if(SpeedBtn == LOW)Axis0Interval = ButtonSpeed;
        else Axis4Interval = map(abs(JoyByVal), 0, 100, MinSpeed, MaxSpeed);

          //Axis4Interval = map(abs(JoyByVal), 0, 100, MinSpeed, MaxSpeed);
          if(JoyByVal > 0 && servo4Pos <= (180 - Axis4Limiter)){
            Axis4Cur = millis();

                  if (Axis4Cur - Axis4Per >= Axis4Interval) {
                    servo4Pos++;
                    servo4.write(servo4Pos);
                    Axis4Per = Axis4Cur;                        
                } 
          }
        if(JoyByVal < 0 && servo4Pos >= (0 + Axis4Limiter)){
             Axis4Cur = millis();

                  if (Axis4Cur - Axis4Per >= Axis4Interval) {
                    servo4Pos--;
                    servo4.write(servo4Pos);
                    Axis4Per = Axis4Cur;                        
                } 
          }
    }

     
      
      if(BtnA_Activ == LOW && servo5Pos<= 180 && catchDetect == false){
             catchDone = false;
              delay(10);  
              getServoCurrent();
              servo5Pos++;
              servo5.write(servo5Pos);
              Serial.println(IsAct);
             if(IsDef < IsAct){
               SmoothIsAct = SmoothIsAct + IsAct;
                SmoothCount++;
             
               
               if((SmoothIsAct/SmoothLevel) >= (IsDef-5) && SmoothLevel == SmoothCount){
                catchDetect = true;
                servo5Pos = servo5Pos - CatchStrength;
                servo5.write(servo5Pos);
               
               
               Serial.print("Smooth: ");
                Serial.println((SmoothIsAct/SmoothLevel));
                SmoothIsAct = 0;
                SmoothCount = 0;
               }

          }
          if(catchDetect == true){
          catchDone = true;
          Serial.println("catchDone !!!");
          }
          
      }
     if(BtnA_Activ == HIGH && servo5Pos >= 0){
             catchDone = false;
             catchDetect = false;
             servo5Pos--;
             servo5.write(servo5Pos);
           if(servo5Pos <= 0)catchDone = true;   
          
               }
         
      
   }

   //Program eddited by Miroslav Skoda @2018 ver1.12
//-----------------Coded By AngryFreeMan@2018--------------//
//-------------------Daniel Lenc #DanHKfree----------------//